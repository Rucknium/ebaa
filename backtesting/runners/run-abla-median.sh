#!/bin/bash

# output file
outfile=$1

# dataset to process
infile=$2

# algo args
excessiveblocksize=$3
n0=$4
alpha0_xB7=$5
windowLength=$6

tmpsim=$(mktemp)

awk -F ',' '{OFS=",";print $3}' "$infile" \
| ../../implementation-c/bin/abla-median -excessiveblocksize $excessiveblocksize -ablaconfig $n0,$alpha0_xB7,$windowLength \
> $tmpsim

tmpact=$(mktemp)
tmpeb=$(mktemp)
tmpcf=$(mktemp)

awk -F ',' '{OFS=",";print $1,$3,$3}' "$infile" | ../../implementation-c/bin/aggregate -windowlength 144 | awk -F ',' '{OFS=",";print $1,$2,$3,$4,$5,$6,$7,$8,$9,$10}' > $tmpact
awk -F ',' '{OFS=",";print $1,$2,$3}' $tmpsim | ../../implementation-c/bin/aggregate -windowlength 144 | awk -F ',' '{OFS=",";print $1,$11,$12,$13,$14}' > $tmpeb
awk -F ',' '{OFS=",";print $1,$2,$4}' $tmpsim | ../../implementation-c/bin/aggregate -windowlength 144 | awk -F ',' '{OFS=",";print $1,$11,$12}' > $tmpcf
sed -i '1 s/openExcessiveBlockSize/blocksizeLimit/' $tmpeb
sed -i '1 s/closeExcessiveBlockSize/blocksizeLimit/' $tmpeb
sed -i '1 s/openExcessiveBlockSize/medianBlockSize/' $tmpcf
sed -i '1 s/closeExcessiveBlockSize/medianBlockSize/' $tmpcf
join --header  --nocheck-order -t, -1 1 -2 1 $tmpact $tmpeb >$tmpsim
join --header  --nocheck-order -t, -1 1 -2 1 $tmpsim $tmpcf >$outfile

rm $tmpact
rm $tmpsim
rm $tmpeb
rm $tmpcf
