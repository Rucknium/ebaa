block=$1
limit=$2
while [ $block -le $((limit-100+1)) ]
do
    res=$(curl -s 'https://api.blockchair.com/doge/blocks?s=id(asc)&limit=100&q=id('$block'..)')
    rows=$(echo "$res" | jq -r '.data | map([.id, (.time | sub("\\ ";"T") + "Z" | fromdateiso8601 - 3600), .size] | join(",")) | join("\n")')
    readarray <<<"$rows" rows
    if [ ${#rows[@]} == 100 ]
    then
        printf "%s" "${rows[@]}"
        block=$((block+100))
    else
        echo $res > /dev/stderr
    fi
    sleep 2
done

