#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>

#define B7 1ull<<7 // 128

typedef __uint128_t uint128_t;

typedef struct {
    uint64_t n0;
    uint64_t y0;
    uint64_t alpha0_xB7;
    uint64_t epsilon0;
    uint64_t windowLength;
} configABLA_t;

typedef struct {
    uint64_t blockHeight;
    uint64_t blockSize;
} datumBS_t;

typedef struct {
    uint64_t blockHeight;
    uint64_t controlBlockSize;
    datumBS_t *windowBlockSizes;
} datumABLA_t;

uint64_t muldiv(uint64_t x, uint64_t y, uint64_t z) {
    uint128_t res;
    res = ((uint128_t) x * (uint128_t) y) / (uint128_t) z;
    assert(res < (uint128_t) UINT64_MAX);
    return (uint64_t) res;
}

uint64_t min(uint64_t x, uint64_t y) {
    return x < y ? x : y;
}

uint64_t max(uint64_t x, uint64_t y) {
    return x < y ? y : x;
}

int cmpUint64 (const void * a, const void * b) {
    if (*(uint64_t*)a == *(uint64_t*)b)
        return 0;
    else
        return *(uint64_t*)a < *(uint64_t*)b ? -1 : 1;
}

int cmpBS (const void * a, const void * b) {
    if (((datumBS_t*)a)->blockSize == ((datumBS_t*)b)->blockSize)
        return 0;
    else
        return ((datumBS_t*)a)->blockSize < ((datumBS_t*)b)->blockSize ? -1 : 1;
}

void initABLA(datumABLA_t *stateABLA, configABLA_t *configABLA)
{
    for (size_t i = 0; i < configABLA->windowLength; i++) {
        stateABLA->windowBlockSizes[i].blockHeight = i;
        stateABLA->windowBlockSizes[i].blockSize = configABLA->epsilon0;
    }
    stateABLA->controlBlockSize = configABLA->epsilon0;
    stateABLA->blockHeight = configABLA->windowLength;
}

void nextABLA(datumABLA_t *stateABLA, configABLA_t *configABLA, uint64_t blockSize)
{
    uint64_t lowHeight = stateABLA->blockHeight - configABLA->windowLength;
    datumBS_t *lowBS = NULL;
    for (size_t i = 0; i < configABLA->windowLength; i++) {
        if (stateABLA->windowBlockSizes[i].blockHeight == lowHeight)
            lowBS = &stateABLA->windowBlockSizes[i];
    }
    lowBS->blockHeight = stateABLA->blockHeight;
    stateABLA->blockHeight++;
    if (stateABLA->blockHeight > configABLA->n0) {
        lowBS->blockSize = min(blockSize, muldiv(stateABLA->controlBlockSize, configABLA->alpha0_xB7, B7));
        qsort(stateABLA->windowBlockSizes, configABLA->windowLength, sizeof(datumBS_t), cmpBS);
        // median
        if (configABLA->windowLength % 2 == 0)
            stateABLA->controlBlockSize = (stateABLA->windowBlockSizes[configABLA->windowLength / 2u - 1].blockSize +
                                           stateABLA->windowBlockSizes[configABLA->windowLength / 2u].blockSize) / 2u;
        else
            stateABLA->controlBlockSize = stateABLA->windowBlockSizes[configABLA->windowLength / 2u].blockSize;
        stateABLA->controlBlockSize = max(stateABLA->controlBlockSize, configABLA->epsilon0);
    }
}

#define ABLA_USAGE "Usage: ./abla-median <-excessiveblocksize 1000000> <-ablaconfig n0,alpha0_xB7,windowLength>\n"
int main (int argc, char *argv[])
{
    uint64_t configExcessiveBlockSize = 0;
    configABLA_t configABLA = {0};
    datumABLA_t stateABLA = {0};

    // Parse arguments
    if (argc < 3) {
        fprintf(stderr, ABLA_USAGE);
        return 1;
    }
    if (!strcmp(argv[1], "-excessiveblocksize")) {
        if (sscanf(argv[2], "%lu", &configExcessiveBlockSize) < 1) {
            fprintf(stderr, "Error, -excessiveblocksize argument missing.\n");
            return 1;
        }
    }
    else {
        fprintf(stderr, "Error, failed parsing -excessiveblocksize argument.\n");
        return 1;
    }
    if (argc > 4 && ! strcmp(argv[3], "-ablaconfig")) {
        if (sscanf(argv[4], "%lu,%lu,%lu", &configABLA.n0, &configABLA.alpha0_xB7, &configABLA.windowLength) < 3) {
            fprintf(stderr, "Error, failed parsing -ablaconfig arguments.\n");
            return 1;
        }
        configABLA.y0 = configExcessiveBlockSize;
        configABLA.epsilon0 = muldiv(configABLA.y0, B7, configABLA.alpha0_xB7);
    }
    else {
        fprintf(stderr, "Error, -ablaconfig argument missing.\n");
        return 1;
    }

    stateABLA.windowBlockSizes = malloc(configABLA.windowLength * sizeof(datumBS_t));
    initABLA(&stateABLA, &configABLA);
    configABLA.n0 += configABLA.windowLength;

    // Calculate and print
    uint64_t blockSize;
    uint64_t excessiveBlockSize;
    while (scanf("%lu", &blockSize) == 1) {
        excessiveBlockSize = max(muldiv(stateABLA.controlBlockSize, configABLA.alpha0_xB7, B7), configABLA.y0);
        blockSize = min(blockSize, excessiveBlockSize);
        nextABLA(&stateABLA, &configABLA, blockSize);
        excessiveBlockSize = max(muldiv(stateABLA.controlBlockSize, configABLA.alpha0_xB7, B7), configABLA.y0);
        printf("%lu,%lu,%lu,%lu\n", stateABLA.blockHeight - configABLA.windowLength - 1, blockSize, excessiveBlockSize, stateABLA.controlBlockSize);
    }

    free(stateABLA.windowBlockSizes);
    return 0;
}
