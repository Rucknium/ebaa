#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>
#include <inttypes.h>

typedef __uint128_t uint128_t;

// Utility functions

uint64_t muldiv(uint64_t x, uint64_t y, uint64_t z) {
    assert(z != 0);
    uint128_t res = ((uint128_t) x * y) / z;
    assert(res <= (uint128_t) UINT64_MAX);
    return (uint64_t) res;
}

uint64_t min(uint64_t x, uint64_t y) {
    return x < y ? x : y;
}

uint64_t max(uint64_t x, uint64_t y) {
    return x < y ? y : x;
}

// Constant 2^7, used as fixed precision for algorithm's "asymmetry
// factor" configuration value, e.g. we will store the real number 1.5
// as integer 192 so when we want to multiply or divide an integer with
// value of 1.5, we will do muldiv(value, 192, B7) or
// muldiv(value, B7, 192).
#define B7 (1ull<<7) // 2^7 = 128

// Algorithm configuration
typedef struct {
    // Initial control block size value, also used as floor value
    uint64_t epsilon0;
    // Initial elastic buffer size value, also used as floor value
    uint64_t beta0;
    // Initial absolutely-scheduled lower bound value
    uint64_t alpha0;
    // Initial absolutely-scheduled upper bound value
    uint64_t omega0;
    // Last block height which will have the initial block size limit
    uint64_t n0;
    // Reciprocal of control function "forget factor" value
    uint64_t gammaReciprocal;
    // Control function "asymmetry factor" value
    uint64_t zeta_xB7;
    // Reciprocal of elastic buffer decay rate
    uint64_t thetaReciprocal;
    // Elastic buffer "gear factor"
    uint64_t delta;
    // Reciprocal of absolutely-scheduled lower bound growth rate
    uint64_t rhoReciprocal;
    // Reciprocal of absolutely-scheduled upper bound growth rate
    uint64_t phiReciprocal;
    // Boundaries to enforce bit0 for lower, bit1 for upper.
    unsigned int enforceScheduledBoundaries;
} configABLA_t;

// Algorithm's internal state
// Note: limit for the block with blockHeight will be given by
// controlBlockSize + elasticBufferSize
typedef struct {
    // Block height for which the state applies
    uint64_t blockHeight;
    // Control function state
    uint64_t controlBlockSize;
    // Elastic buffer function state
    uint64_t elasticBufferSize;
    // Absolutely-scheduled lower bound state
    uint64_t lowerBound;
    // Absolutely-scheduled upper bound state
    uint64_t upperBound;
} datumABLA_t;

// Calculate the limit for the block to which the algorithm's state
// applies, given algorithm state, and algorithm configuration.
uint64_t getBlockSizeLimit(datumABLA_t *stateABLA)
{
    return (stateABLA->controlBlockSize + stateABLA->elasticBufferSize);
}

// Calculate algorithm's state for the next block (n), given 
// current blockchain tip (n-1) block size, algorithm state, and
// algorithm configuration.
datumABLA_t nextABLAState (uint64_t currentBlockSize, datumABLA_t currentABLAState, configABLA_t *configABLA) {
    datumABLA_t nextABLAState;

    // n = n + 1
    nextABLAState.blockHeight = currentABLAState.blockHeight + 1;

    // use initialization values for block heights 0 to n0 inclusive
    if (nextABLAState.blockHeight <= configABLA->n0) {
        // alpha_n = alpha_0
        nextABLAState.lowerBound = configABLA->alpha0;
        // epsilon_n = epsilon_0
        nextABLAState.controlBlockSize = configABLA->epsilon0;
        // beta_n = beta_0
        nextABLAState.elasticBufferSize = configABLA->beta0;
        // omega_n = omega_0
        nextABLAState.upperBound = configABLA->omega0;
    }
    // algorithmic limit
    else {
        // update absolutely-scheduled lower and upper bounds
        nextABLAState.lowerBound = currentABLAState.lowerBound + currentABLAState.lowerBound / configABLA->rhoReciprocal;
        nextABLAState.upperBound = currentABLAState.upperBound + currentABLAState.upperBound / configABLA->phiReciprocal;

        // control function

        // zeta * x_{n-1}
        uint64_t amplifiedCurrentBlockSize = muldiv(configABLA->zeta_xB7, currentBlockSize, B7);

        // if zeta * x_{n-1} > epsilon_{n-1} then increase
        if (amplifiedCurrentBlockSize > currentABLAState.controlBlockSize) {
            // zeta * x_{n-1} - epsilon_{n-1}
            uint64_t bytesToAdd = amplifiedCurrentBlockSize - currentABLAState.controlBlockSize;

            // zeta * y_{n-1}
            uint64_t amplifiedBlockSizeLimit = muldiv(configABLA->zeta_xB7, currentABLAState.controlBlockSize + currentABLAState.elasticBufferSize, B7);

            // zeta * y_{n-1} - epsilon_{n-1}
            uint64_t bytesMax = amplifiedBlockSizeLimit - currentABLAState.controlBlockSize;

            // zeta * beta_{n-1} * (zeta * x_{n-1} - epsilon_{n-1}) / (zeta * y_{n-1} - epsilon_{n-1})
            uint64_t scalingOffset = muldiv(muldiv(configABLA->zeta_xB7, currentABLAState.elasticBufferSize, B7), bytesToAdd, bytesMax);

            // epsilon_n = epsilon_{n-1} + gamma * (zeta * x_{n-1} - epsilon_{n-1} - zeta * beta_{n-1} * (zeta * x_{n-1} - epsilon_{n-1}) / (zeta * y_{n-1} - epsilon_{n-1}))
            nextABLAState.controlBlockSize = currentABLAState.controlBlockSize + (bytesToAdd - scalingOffset) / configABLA->gammaReciprocal;

            // apply upper bound
            if ((configABLA->enforceScheduledBoundaries & 0x02) > 0) {
                nextABLAState.controlBlockSize = min(nextABLAState.controlBlockSize, currentABLAState.upperBound - currentABLAState.elasticBufferSize);
            }
        }
        // if zeta * x_{n-1} <= epsilon_{n-1} then decrease or no change
        else {
            // epsilon_{n-1} - zeta * x_{n-1}
            uint64_t bytesToRemove = currentABLAState.controlBlockSize - amplifiedCurrentBlockSize;

            // epsilon_{n-1} + gamma * (zeta * x_{n-1} - epsilon_{n-1})
            // rearranged to:
            // epsilon_{n-1} - gamma * (epsilon_{n-1} - zeta * x_{n-1})
            nextABLAState.controlBlockSize = currentABLAState.controlBlockSize - bytesToRemove / configABLA->gammaReciprocal;

            // epsilon_n = max(epsilon_{n-1} + gamma * (zeta * x_{n-1} - epsilon_{n-1}), epsilon_0)
            nextABLAState.controlBlockSize = max(nextABLAState.controlBlockSize, configABLA->epsilon0);
        }

        // elastic buffer function

        // beta_{n-1} * theta
        uint64_t bufferDecay = currentABLAState.elasticBufferSize / configABLA->thetaReciprocal;

        // if zeta * x_{n-1} > epsilon_{n-1} then increase
        if (amplifiedCurrentBlockSize > currentABLAState.controlBlockSize) {
            // (epsilon_{n} - epsilon_{n-1}) * delta
            uint64_t bytesToAdd = (nextABLAState.controlBlockSize - currentABLAState.controlBlockSize) * configABLA->delta;

            // beta_{n-1} - beta_{n-1} * theta + (epsilon_{n} - epsilon_{n-1}) * delta
            nextABLAState.elasticBufferSize = currentABLAState.elasticBufferSize - bufferDecay + bytesToAdd;
        }
        // if zeta * x_{n-1} <= epsilon_{n-1} then decrease or no change
        else {
            // beta_{n-1} - beta_{n-1} * theta
            nextABLAState.elasticBufferSize = currentABLAState.elasticBufferSize - bufferDecay;
        }
        // max(beta_{n-1} - beta_{n-1} * theta + (epsilon_{n} - epsilon_{n-1}) * delta, beta_0) , if zeta * x_{n-1} > epsilon_{n-1}
        // max(beta_{n-1} - beta_{n-1} * theta, beta_0) , if zeta * x_{n-1} <= epsilon_{n-1}
        nextABLAState.elasticBufferSize = max(nextABLAState.elasticBufferSize, configABLA->beta0);

        // apply lower bound
        if ((configABLA->enforceScheduledBoundaries & 0x01) > 0) {
            if (nextABLAState.lowerBound > nextABLAState.controlBlockSize + nextABLAState.elasticBufferSize) {
                nextABLAState.controlBlockSize = nextABLAState.lowerBound - nextABLAState.elasticBufferSize;
            }
        }
    }
    return nextABLAState;
}

#define ABLA_USAGE "Usage: ./abla <-excessiveblocksize 1000000> <-ablaconfig beta0,n0,gammaReciprocal,zeta,thetaReciprocal,delta,alpha0,rhoReciprocal,omega0,phiReciprocal,enforceScheduledBoundaries [-ablacontinue blockHeight_n,elasticBufferSize_{n-1},controlBlockSize_{n-1},lowerBound_{n-1},upperBound_{n-1}]>\n"
int main (int argc, char *argv[])
{
    uint64_t configInitialBlockSizeLimit;
    configABLA_t configABLA;
    datumABLA_t stateABLA;

    // Parse arguments
    if (argc < 3) {
        fprintf(stderr, ABLA_USAGE);
        return 1;
    }
    if (strcmp(argv[1], "-excessiveblocksize") == 0) {
        if (sscanf(argv[2], "%" PRIu64, &configInitialBlockSizeLimit) < 1) {
            fprintf(stderr, "Error, -excessiveblocksize argument missing.\n");
            return 1;
        }
    }
    else {
        fprintf(stderr, "Error, failed parsing -excessiveblocksize argument.\n");
        return 1;
    }
    if (argc > 4 && strcmp(argv[3], "-ablaconfig") == 0) {
        if (sscanf(argv[4], "%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%u", &configABLA.beta0,&configABLA.n0, &configABLA.gammaReciprocal, &configABLA.zeta_xB7, &configABLA.thetaReciprocal, &configABLA.delta, &configABLA.alpha0, &configABLA.rhoReciprocal, &configABLA.omega0, &configABLA.phiReciprocal, &configABLA.enforceScheduledBoundaries) < 11) {
            fprintf(stderr, "Error, failed parsing -ablaconfig arguments.\n");
            return 1;
        }

        // Finalize config
        if (configInitialBlockSizeLimit > configABLA.beta0) {
            configABLA.epsilon0 = configInitialBlockSizeLimit - configABLA.beta0;
        }
        else {
            fprintf(stderr, "Error, initial block size limit sanity check failed.\n");
            return 1;
        }

        // Sanity checks

        if (configABLA.zeta_xB7 < B7 || configABLA.zeta_xB7 > 2 * B7) {
            fprintf(stderr, "Error, zeta sanity check failed.\n");
            return 1;
        }
        if (configABLA.gammaReciprocal == 0) {
            fprintf(stderr, "Error, gammaReciprocal sanity check failed.\n");
            return 1;
        }
        if (configABLA.thetaReciprocal == 0) {
            fprintf(stderr, "Error, thetaReciprocal sanity check failed.\n");
            return 1;
        }
        if ((configABLA.enforceScheduledBoundaries & 0x01) > 0 && configABLA.alpha0 > configABLA.epsilon0 + configABLA.beta0) {
            fprintf(stderr, "Error, invalid alpha0 configuration. Can't be above initial blocksize limit.\n");
            return 1;
        }
        if ((configABLA.enforceScheduledBoundaries & 0x02) > 0 && configABLA.omega0 < configABLA.epsilon0 + configABLA.beta0) {
            fprintf(stderr, "Error, invalid omega0 configuration. Can't be below initial blocksize limit.\n");
            return 1;
        }
        if (configABLA.rhoReciprocal == 0) {
            fprintf(stderr, "Error, rhoReciprocal sanity check failed.\n");
            return 1;
        }
        if (configABLA.phiReciprocal == 0) {
            fprintf(stderr, "Error, phiReciprocal sanity check failed.\n");
            return 1;
        }
        if (configABLA.rhoReciprocal <= configABLA.phiReciprocal) {
            fprintf(stderr, "Error, rhoReciprocal must be greater than phiReciprocal.\n");
            return 1;
        }

        // If we want to pick up from some height N instead of starting from height 0
        // then we must set the height & algorithm's state here.
        if (argc > 8 && strcmp(argv[5], "-ablacontinue") == 0) {
            if (sscanf(argv[8], "%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64, &stateABLA.blockHeight, &stateABLA.elasticBufferSize, &stateABLA.controlBlockSize, &stateABLA.lowerBound, &stateABLA.upperBound) < 5) {
                fprintf(stderr, "Error, failed parsing -ablacontinue arguments.\n");
                return 1;
            }
            if (stateABLA.controlBlockSize < configABLA.epsilon0) {
                fprintf(stderr, "Error, invalid controlBlockSize state. Can't be below initialization value.\n");
                return 1;
            }
            if (stateABLA.elasticBufferSize < configABLA.beta0) {
                fprintf(stderr, "Error, invalid elasticBufferSize state. Can't be below initialization value.\n");
                return 1;
            }
            if ((configABLA.enforceScheduledBoundaries & 0x01) > 0 && stateABLA.controlBlockSize + stateABLA.elasticBufferSize < stateABLA.lowerBound) {
                fprintf(stderr, "Error, invalid blockSizeLimit state. Can't be below lower bound.\n");
                return 1;
            }
            if ((configABLA.enforceScheduledBoundaries & 0x02) > 0 && stateABLA.controlBlockSize + stateABLA.elasticBufferSize > stateABLA.upperBound) {
                fprintf(stderr, "Error, invalid blockSizeLimit state. Can't be above upper bound.\n");
                return 1;
            }
        }
        else {
            // Initialize state for height 0
            stateABLA.blockHeight = 0;
            stateABLA.lowerBound = configABLA.alpha0;
            stateABLA.controlBlockSize = configABLA.epsilon0;
            stateABLA.elasticBufferSize = configABLA.beta0;
            stateABLA.upperBound = configABLA.omega0;
        }
    }
    else {
        fprintf(stderr, "Error, -ablaconfig argument missing.\n");
        return 1;
    }

    // Calculate and print
    uint64_t blockSize;
    uint64_t blockSizeLimit;
    while (scanf("%" PRIu64, &blockSize) == 1) {
        // blockSize can't be greater than the limit, but if we're
        // testing against some dataset then we must clip it here.
        blockSize = min(blockSize, getBlockSizeLimit(&stateABLA));
        // calculate the next block's algorithm state
        stateABLA = nextABLAState(blockSize, stateABLA, &configABLA);
        blockSizeLimit = getBlockSizeLimit(&stateABLA);
        printf("%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64 ",%" PRIu64 "\n", stateABLA.blockHeight - 1, blockSize, blockSizeLimit, stateABLA.elasticBufferSize, stateABLA.controlBlockSize, stateABLA.lowerBound, stateABLA.upperBound);
    }

    return 0;
}
